using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using ScrumBoard.DataAccess;
using ScrumBoard.Extensions;
using ScrumBoard.Models.Entities;
using ScrumBoard.Models.Statistics;
using ScrumBoard.Services;
using ScrumBoard.Tests.Integration.Infrastructure;
using ScrumBoard.Tests.Util;
using Xunit;
using Xunit.Abstractions;

namespace ScrumBoard.Tests.Integration.Services;

public class ProjectStatsServiceTest : BaseIntegrationTestFixture
{    
    private readonly IProjectStatsService _projectStatsService;

    private Project _project;
    private User _user1;
    private User _user2;
    private WorklogTag _worklogTag;

    private UserStory _userStory;
    private UserStory _userStory2;
    private UserStory _userStory3;

    private Sprint _sprint;
    private Sprint _sprint2;
    private Sprint _sprint3;

    private UserStoryTask _sprint1Task1;
    private UserStoryTask _sprint2Task1;
    private UserStoryTask _sprint2Task2;
    private UserStoryTask _sprint3Task1;

    private WorklogEntry _worklogEntry;
    private WorklogEntry _worklogEntry2; 
    private WorklogEntry _worklogEntryPair;
    private WorklogEntry _worklogEntrySprint2; 
    private WorklogEntry _worklogEntrySprint2Entry2;

    public ProjectStatsServiceTest(TestWebApplicationFactory factory, ITestOutputHelper outputHelper) : base(factory, outputHelper)
    {
        _projectStatsService = ServiceProvider.GetRequiredService<IProjectStatsService>();
    }

    protected override async Task SeedSampleDataAsync(DatabaseContext dbContext)
    {
        _project = FakeDataGenerator.CreateFakeProject();
        _user1 = new User { Id = 101, FirstName = "John", LastName = "Smith" };
        _user2 = new User { Id = 102, FirstName = "Johnny", LastName = "Smithy" };
        _worklogTag = FakeDataGenerator.CreateWorklogTag();
        
        await dbContext.Users.AddAsync(_user1);
        await dbContext.Projects.AddAsync(_project);
        await dbContext.WorklogTags.AddAsync(_worklogTag);

        await dbContext.ProjectUserMemberships.AddAsync(new ProjectUserMembership
        {
            Role = ProjectRole.Developer, UserId = _user1.Id, ProjectId = _project.Id, User = _user1, Project = _project
        });
        await dbContext.ProjectUserMemberships.AddAsync(new ProjectUserMembership
        {
            Role = ProjectRole.Developer, UserId = _user2.Id, ProjectId = _project.Id, User = _user2, Project = _project
        });
        
        _sprint = FakeDataGenerator.CreateFakeSprintWithDatabaseProject(_project);
        _sprint2 = FakeDataGenerator.CreateFakeSprintWithDatabaseProject(_project);
        _sprint3 = FakeDataGenerator.CreateFakeSprintWithDatabaseProject(_project);
        await dbContext.AddRangeAsync(_sprint, _sprint2, _sprint3);

        _userStory = FakeDataGenerator.CreateFakeUserStoryWithDatabaseSprint(_sprint);
        _userStory2 = FakeDataGenerator.CreateFakeUserStoryWithDatabaseSprint(_sprint2);
        _userStory3 = FakeDataGenerator.CreateFakeUserStoryWithDatabaseSprint(_sprint3);
        
        await dbContext.AddRangeAsync(_userStory, _userStory2, _userStory3);

        _sprint1Task1 = FakeDataGenerator.CreateFakeTaskForDatabaseUserStory(_userStory);
        _sprint2Task1 = FakeDataGenerator.CreateFakeTaskForDatabaseUserStory(_userStory2);
        _sprint2Task2 = FakeDataGenerator.CreateFakeTaskForDatabaseUserStory(_userStory2);
        _sprint3Task1 = FakeDataGenerator.CreateFakeTaskForDatabaseUserStory(_userStory3);
        await dbContext.AddRangeAsync(_sprint1Task1, _sprint2Task1, _sprint2Task2, _sprint3Task1);
        
        _worklogEntry = new WorklogEntry
        {
            Description = "Test Worklog Entry",
            UserId = _user1.Id,
            TaskId = _sprint1Task1.Id,
            TaggedWorkInstances = new [] { 
                FakeDataGenerator.CreateFakeTaggedWorkInstanceForDatabaseWorklogTag(_worklogTag, new TimeSpan(2)) 
            },
        };
        _worklogEntryPair = new WorklogEntry
        {
            Description = "This has a pair",
            UserId = _user1.Id,
            TaskId = _sprint1Task1.Id,
            PairUserId = _user2.Id,
            TaggedWorkInstances = new [] { 
                FakeDataGenerator.CreateFakeTaggedWorkInstanceForDatabaseWorklogTag(_worklogTag, new TimeSpan(1)) 
            },
        };
        _worklogEntry2 = new WorklogEntry
        {
            Description = "This belongs in sprint 2",
            UserId = _user1.Id,
            TaskId = _sprint2Task1.Id,
            TaggedWorkInstances = new [] { 
                FakeDataGenerator.CreateFakeTaggedWorkInstanceForDatabaseWorklogTag(_worklogTag, new TimeSpan(3)) 
            },
        };
        _worklogEntrySprint2 = new WorklogEntry
        {
            Description = "This is for user 2",
            UserId = _user2.Id,
            TaskId = _sprint2Task1.Id,
            TaggedWorkInstances = new [] { 
                FakeDataGenerator.CreateFakeTaggedWorkInstanceForDatabaseWorklogTag(_worklogTag, new TimeSpan(1)) 
            },
        };
        _worklogEntrySprint2Entry2 = new WorklogEntry
        {
            Description = "This is user2, working on another task",
            UserId = _user2.Id,
            TaskId = _sprint2Task2.Id,
            TaggedWorkInstances = new [] { 
                FakeDataGenerator.CreateFakeTaggedWorkInstanceForDatabaseWorklogTag(_worklogTag, new TimeSpan(3)) 
            }
        };
        
        await dbContext.AddRangeAsync(_worklogEntry, _worklogEntryPair, _worklogEntry2, _worklogEntrySprint2, _worklogEntrySprint2Entry2);
        await dbContext.SaveChangesAsync();
    }
    
    [Fact]
    public async Task GetTimePerUserSprint_SprintHasTimeLogged_ReturnsTimePerUser() 
    {
        var result = await _projectStatsService.GetTimePerUser(_project.Id, _sprint.Id);
        var durationTotalHoursUser1 = getTotalTimeOnWorklogs([_worklogEntry, _worklogEntryPair], _user1).TotalHours;
        var durationTotalHoursUser2 = getTotalTimeOnWorklogs([_worklogEntry, _worklogEntryPair], _user2).TotalHours;        
        var expected = new StatsBar(
            new List<ProgressBarChartSegment<double>> { new(0, _user1.GetFullName(), durationTotalHoursUser1), new(1, _user2.GetFullName(), durationTotalHoursUser2)  }, 
            durationTotalHoursUser1 + durationTotalHoursUser2
        );
        result.Should().BeEquivalentTo(expected);
    }

    private static TimeSpan getTotalTimeOnWorklogs(IEnumerable<WorklogEntry> worklogEntries, User user)
    {
        return worklogEntries.Sum(x => x.TaggedWorkInstances.Where(x => x.WorklogEntry.UserId == user.Id).Sum(x => x.Duration));
    }

    [Fact]
    public async Task GetTimePerUserProject_ProjectHasTimeLogged_ReturnsTimePerUser() 
    {
        var result = await _projectStatsService.GetTimePerUser(_project.Id);
        var durationTotalHoursUser1 = getTotalTimeOnWorklogs([_worklogEntry2, _worklogEntry, _worklogEntryPair, _worklogEntrySprint2, _worklogEntrySprint2Entry2], _user1).TotalHours;
        var durationTotalHoursUser2 = getTotalTimeOnWorklogs([_worklogEntry2, _worklogEntry, _worklogEntryPair, _worklogEntrySprint2, _worklogEntrySprint2Entry2], _user2).TotalHours;
        var expected = new StatsBar(
            new List<ProgressBarChartSegment<double>> { new(0, _user1.GetFullName(), durationTotalHoursUser1), new(1, _user2.GetFullName(), durationTotalHoursUser2)  }, 
            durationTotalHoursUser1 + durationTotalHoursUser2
        );
        result.Should().BeEquivalentTo(expected);
    }

    [Fact]
    public async Task GetStoriesWorkedPerUserSprint_SprintHasUsersWorked_ReturnsStoriesWorkedPerUser() 
    {
        var result = await _projectStatsService.GetStoriesWorkedPerUser(_project.Id, _sprint.Id);
        var expected = new StatsBar(
            new List<ProgressBarChartSegment<double>> {  new(0, _user1.GetFullName(), 1), new(1, _user2.GetFullName(), 0)  }, 
            1
        );
        
        result.Should().BeEquivalentTo(expected);
    }

    [Fact]
    public async Task GetStoriesWorkedPerUserProject_ProjectHasUsersWorked_ReturnsStoriesWorkedPerUser()
    {
        var result = await _projectStatsService.GetStoriesWorkedPerUser(_project.Id);
        var expected = new StatsBar(
            new List<ProgressBarChartSegment<double>> { new(0, _user1.GetFullName(), 2), new(1, _user2.GetFullName(), 1)  }, 
            3
        );
        result.Should().BeEquivalentTo(expected);
    }

    [Fact]
    public async Task GetTasksWorkedOnPerUserSprint_SprintHasUsersWorked_ReturnsTasksWorkedPerUser() 
    {
        var result = await _projectStatsService.GetTasksWorkedOnPerUser(_project.Id, _sprint.Id);

        var expected = new StatsBar(
            new List<ProgressBarChartSegment<double>> { new(0, _user1.GetFullName(), 1), new(1, _user2.GetFullName(), 0) }, 
            1
        );
        result.Should().BeEquivalentTo(expected);
    }
    
    [Fact]
    public async Task GetTasksWorkedOnPerUserSprint_SprintHasMultipleTasksWorkedPerUser_ReturnsTasksWorkedPerUser() 
    {
        var result = await _projectStatsService.GetTasksWorkedOnPerUser(_project.Id, _sprint2.Id);

        var expected = new StatsBar(
            new List<ProgressBarChartSegment<double>> { new(0, _user1.GetFullName(), 1), new(1, _user2.GetFullName(), 2) }, 
            3
        );
        result.Should().BeEquivalentTo(expected);
    }
    
    [Fact]
    public async Task GetTasksWorkedOnPerUserSprint_NoTasksWorkedOn_ReturnsTasksWorkedPerUser() 
    {
        var result = await _projectStatsService.GetTasksWorkedOnPerUser(_project.Id, _sprint3.Id);

        var expected = new StatsBar(
            new List<ProgressBarChartSegment<double>> { new(0, _user1.GetFullName(), 0), new(1, _user2.GetFullName(), 0) }, 
            0
        );
        result.Should().BeEquivalentTo(expected);
    }
}