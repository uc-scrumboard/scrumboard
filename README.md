# ScrumBoard (AKA LENS Resurrected)

This project represents the first open-source release of the ScrumBoard project developed by members of Te Whare Wananga o Waitaha | University of Canterbury, Ōtautahi Christchurch, Aotearoa New Zealand.

## Purpose

The ScrumBoard is a SCrum-compliant project management tool used to teach Scrum in project-based learning.

The tool offers the following features:

- backlog with stories, acceptance criteria, and tasks
- sprint definition
- various reports, e.g., burdown, cumulative flow, worklogs, personal summary
- ability to perform a sprint review, checking all ACs
- forms for peer-feedback, self-reflection, surveys
- ability to log time spent on tasks, or formal events (e.g., daily scrum, reviews, retrospective)
- ability to reference gitlab commits to worklogs
- ability to embed course documentation hosted on a gitlab repository as markdown files
- permission management and roles

## Quickstart

The `dev_utils.sh` is a helpful script to get up and running for ScrumBoard development. In short, it automatically builds and loads external dependencies like the identity provider / user dashboard and mariadb database. If you're just going to be working on the ScrumBoard project alone, you can easily get underway by running the following commands (`docker` and `docker compose` must be installed on your machine first):

First, for building the necessary Docker images:
```shell
./dev_utils.sh init
```

Second, for starting the dependent and auxiliary services:
```shell
./dev_utils.sh setup-scrumboard-dev
```

From this point you have everything you need to run the ScrumBoard. Do this either through your IDE, or via the commands:
```shell
cd ScrumBoard
dotnet watch
```

To clean up the services later, run the following command:
```shell
./dev_utils.sh tear-down
```

## Branching guidelines

- New feature branches should be branched off directly from `main` and named in `PascalCase`
- All merge requests should be targeted at `main`

## CI

GitLab CI is not currently configured for the open-source repository.

## Contact us - Contributing

If you are interested in contributing or if you want more information, you can contact the product owner [Fabian Gilson](mailto:fabian.gilson@canterbury.ac.nz) or the lead developer [Matthew Minish](mailto:matthew.minish@pg.canterbury.ac.nz).
