using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using ScrumBoard.Models;
using ScrumBoard.Models.Entities;
using ScrumBoard.Services;

namespace ScrumBoard.Shared.Marking;

public partial class SprintSummaryTableRow : BaseProjectScopedComponent
{
    [CascadingParameter(Name = "Sprint")] 
    public Sprint Sprint { get; set; }

    [Parameter] 
    public MarkingTableMetric Metric { get; set; }

    [Parameter] 
    public User SelectedUser { get; set; }
    
    [Parameter] 
    public IList<DateOnly> WeekStartDatesAscending { get; set; }
    
    [Parameter]
    public bool ShowTotal { get; set; }

    [Inject] 
    private IMarkingStatsService MarkingStatsService { get; set; }

    private IEnumerable<WeeklyValue> _weeklyValues;
    private IEnumerable<MetricDisplayByWeek> _displayByWeek = new List<MetricDisplayByWeek>();
    private string _displayForTotal;
    private const string ZeroDisplay = "00:00:00";
    
    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        await GenerateSummary();
    }

    private async Task GenerateSummary()
    {
        var userToFetchFor = Self;
        if (RoleInCurrentProject == ProjectRole.Leader)
        {
            userToFetchFor = SelectedUser ?? Self;
        }

        await GetHoursByWeek(userToFetchFor);
    }

    private async Task GetHoursByWeek(User userToFetchFor)
    {
        _weeklyValues = Metric switch
        {
            MarkingTableMetric.Overhead => await MarkingStatsService.GetOverheadByWeek(userToFetchFor.Id, Project.Id, Sprint?.Id),
            MarkingTableMetric.StoryHours => await MarkingStatsService.GetStoryHoursByWeek(userToFetchFor.Id, Project.Id, Sprint?.Id),
            MarkingTableMetric.TestHours => await MarkingStatsService.GetTestHoursByWeek(userToFetchFor.Id, Project.Id, Sprint?.Id),
            MarkingTableMetric.AvgLogDuration => await MarkingStatsService.GetAvgWorkLogDurationByWeek(userToFetchFor.Id, Project.Id, Sprint?.Id),
            MarkingTableMetric.ShortestWorklogDuration => await MarkingStatsService.GetShortestWorklogDurationByWeek(userToFetchFor.Id, Project.Id, Sprint?.Id),
            MarkingTableMetric.TestingContribution => await MarkingStatsService.GetTestingContributionByWeek(userToFetchFor.Id, Project.Id, Sprint?.Id),
            _ => throw new NotSupportedException()
        };

        SetDisplayByWeek();
        _displayForTotal = await GetDisplayForTotal(userToFetchFor);
    }

    /// <summary>
    /// Processes a list of weeklyTimeSpans with a given set of week start dates and a metric, and sets _displayByWeek
    /// to a list of MetricDisplayByWeek objects, which contain the string value to display in the MarkingSummaryTable,
    /// as well as a dictionary mapping sprint names to the amount of time logged for that week and sprint, if there are
    /// at least two sprints in the given week.
    /// </summary>
    private void SetDisplayByWeek()
    {
        var weeklyTimeDisplays = new List<MetricDisplayByWeek>();

        foreach (var weekStart in WeekStartDatesAscending)
        {
            var timeThisWeek = _weeklyValues.Where(x => x.WeekStart == weekStart).ToList();

            var displayAmount = Metric switch
            {
                MarkingTableMetric.ShortestWorklogDuration => timeThisWeek.Any()
                    ? TimeSpan.FromTicks((long)timeThisWeek.Min(x => x.Value)).ToString()
                    : ZeroDisplay,
                MarkingTableMetric.TestingContribution => Math.Round(timeThisWeek.Sum(x => x.Value) * 100) + "%",
                _ => TimeSpan.FromSeconds(Math.Round(TimeSpan.FromTicks((long)timeThisWeek.Sum(x => x.Value)).TotalSeconds)).ToString()
            };

            Dictionary<string, string> timeBySprint = new();
            
            if (timeThisWeek.Count(x => x.Value > 0) > 1 && Metric is not MarkingTableMetric.AvgLogDuration)
            {
                foreach (var weeklyTimeSpan in timeThisWeek)
                {
                    timeBySprint.Add(weeklyTimeSpan.SprintName, TimeSpan.FromTicks((long)weeklyTimeSpan.Value).ToString());
                }
            }
            
            weeklyTimeDisplays.Add(
                 new MetricDisplayByWeek
                 {
                     WeekStart = weekStart,
                     TimeForWeek = displayAmount,
                     TimeBySprint = timeBySprint
                 });
        }

        _displayByWeek = weeklyTimeDisplays;
    }

    private async Task<string> GetDisplayForTotal(User userToFetchFor)
    {
        switch (Metric)
        {
            case MarkingTableMetric.Overhead:
            case MarkingTableMetric.StoryHours:
            case MarkingTableMetric.TestHours:
                return TimeSpan.FromTicks((long)_weeklyValues.Sum(wts => wts.Value)).ToString();
            case MarkingTableMetric.TestingContribution:
                var testHoursByWeek =
                    await MarkingStatsService.GetTestHoursByWeek(userToFetchFor.Id, Project.Id, Sprint?.Id);
                var storyHoursByWeek =
                    await MarkingStatsService.GetDevHoursByWeek(userToFetchFor.Id, Project.Id,
                        Sprint?.Id);
                var testTotal = testHoursByWeek.Sum(x => x.Value);
                var storyTotal = storyHoursByWeek.Sum(x => x.Value);
                return testTotal != 0 ? Math.Round(testTotal / (testTotal + storyTotal) * 100) + "%" : "0%";
            case MarkingTableMetric.AvgLogDuration:
            case MarkingTableMetric.ShortestWorklogDuration:
            default:
                return null;
        }
    }
}