namespace ScrumBoard.Utils;

public static class WorklogTagUtils
{
    public static readonly string[] TestWorklogNames = ["Test", "Testmanual"];
    public static readonly string[] DevWorklogNames = ["Feature", "Fix", "Refactor", "Reengineer"];
    public static readonly string[] StoryWorklogNames = []; // allow all worklog tags
    public static readonly string[] AllWorklogNames = ["Feature", "Test", "TestManual", "Fix", "Chore", "Refactor", "Reengineer", "Spike", "Review", "Document"];
    public static readonly string[] TagsThatNeedLinkedCommits = ["Feature", "Test", "Fix", "Chore", "Refactor", "Reengineer"];
    public static readonly string[] TagsThatNeedLinkedUrls = ["TestManual", "Spike"];
}