using System;
using System.Globalization;

namespace ScrumBoard.Utils;

public static class DateUtils {
    public static int GetIsoWeek(this DateOnly dateOnly) => ISOWeek.GetWeekOfYear(dateOnly.ToDateTime(new TimeOnly()));
}
