using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ScrumBoard.DataAccess;
using ScrumBoard.Filters;
using ScrumBoard.Models.Entities;
using ScrumBoard.Repositories;
using ScrumBoard.Utils;

namespace ScrumBoard.Services;

public interface IMarkingStatsService
{
    /// <summary>
    /// Calculates how much overhead a student logged in each week of a project/sprint. If no sprintId is provided, we
    /// return a list of WeeklyTimeSpans, which represents the amount of time logged in each active week of the project.
    /// If there is a sprintId, we return this list for only the weeks from the sprint start to the sprint end, or the
    /// last day the student logged time for that sprint, if it's after the sprint date.
    /// </summary>
    /// <param name="userId">The user to sum overhead logs for</param>
    /// <param name="projectId">The project we are calculating overhead for</param>
    /// <param name="sprintId">An optional Id for a sprint to calculate overhead for</param>
    /// <returns>A list of WeeklyValue objects</returns>
    Task<IEnumerable<WeeklyValue>> GetOverheadByWeek(long userId, long projectId, long? sprintId = null);
    
    /// <summary>
    /// Calculates the average duration of work logs in each week of a project/sprint.
    /// If there are no work logs in a week, the average duration is 0.
    /// </summary>
    /// <param name="userId">The user to sum overhead logs for</param>
    /// <param name="projectId">The project we are calculating overhead for</param>
    /// <param name="sprintId">An optional Id for a sprint to calculate overhead for</param>
    /// <returns>A list of WeeklyValue objects</returns>
    Task<IEnumerable<WeeklyValue>> GetAvgWorkLogDurationByWeek(long userId, long projectId, long? sprintId);

    /// <summary>
    /// Same as GetOverheadByWeek, but for story hours.
    /// </summary>
    /// <param name="userId">The user to sum overhead logs for</param>
    /// <param name="projectId">The project we are calculating overhead for</param>
    /// <param name="sprintId">An optional Id for a sprint to calculate overhead for</param>
    /// <returns>A list of WeeklyValue objects</returns>
    Task<IEnumerable<WeeklyValue>> GetStoryHoursByWeek(long userId, long projectId, long? sprintId = null);
    
    /// <summary>
    /// Same as GetOverheadByWeek, but for test hours. Includes work logs with #test and #testmanual tags.
    /// </summary>
    /// <param name="userId">The user to sum overhead logs for</param>
    /// <param name="projectId">The project we are calculating overhead for</param>
    /// <param name="sprintId">An optional Id for a sprint to calculate overhead for</param>
    /// <returns>A list of WeeklyValue objects</returns>
    Task<IEnumerable<WeeklyValue>> GetTestHoursByWeek(long userId, long projectId, long? sprintId = null);

    Task<IEnumerable<WeeklyValue>> GetDevHoursByWeek(long userId, long projectId, long? sprintId = null);

    /// <summary>
    /// Calculates the timespan of the shortest worklog that a student created in each week of a sprint. If no sprintId is provided, we
    /// return a list of WeeklyTimeSpans, which represents the amount of time logged in each active week of the project.
    /// If there is a sprintId, we return this list for only the weeks from the sprint start to the sprint end, or the
    /// last day the student logged time for that sprint, if it's after the sprint date.
    /// </summary>
    /// <param name="userId">The user to find the shortest worklogs for</param>
    /// <param name="projectId">The project that the worklogs should belong to</param>
    /// <param name="sprintId">Optional id of a sprint to find the shortest worklogs for</param>
    /// <returns>A list of WeeklyValue objects</returns>
    Task<IEnumerable<WeeklyValue>> GetShortestWorklogDurationByWeek(long userId, long projectId, long? sprintId = null);
    
    /// <summary>
    /// Calculates the proportion of the user's development time spent on testing, using the equation
    /// (test+testmanual)/(feature+fix+refactor+reengineer+test+testmanual). 
    /// </summary>
    /// <param name="userId">The user to get the testing contributions for</param>
    /// <param name="projectId">The project that the worklogs should belong to</param>
    /// <param name="sprintId">The optional ID of a sprint the worklogs should belong to</param>
    /// <returns>A list of WeeklyValue objects, which represent the percentage of the user's time spent on testing each week</returns>
    Task<IEnumerable<WeeklyValue>> GetTestingContributionByWeek(long userId, long projectId, long? sprintId);

    /// <summary>
    /// Calculates the range of ISO weeks that may have overhead logged in them for either a whole project or a specific sprint.
    /// Each sprint's date range starts on its start date and ends on either the sprint end date, or the last date anyone logged overhead against the sprint.
    /// </summary>
    /// <param name="sprints">The sprints to calculate date ranges for</param>
    /// <param name="sprintId">The optional sprint to calculate date ranges for</param>
    /// <returns>A list of ISO week numbers representing all weeks in the project/sprint</returns>
    Task<IList<DateOnly>> CalculateDateRangesForSprintOrSprints(IEnumerable<Sprint> sprints, long? sprintId = null);

    /// <summary>
    /// Returns either the date the sprint ended, or the date of the last overhead log for the sprint, whichever is last.
    /// </summary>
    /// <param name="sprint">The sprint to compute for</param>
    /// <returns>The later of the two dates as a DateOnly</returns>
    Task<DateOnly> GetSprintEndOrLastLog(Sprint sprint);
}

public struct WeeklyValue
{
    public DateOnly WeekStart { get; init; }
    public double Value { get; init; }
    public long? SprintId { get; init; }
    public string SprintName { get; init; }
}

public class MarkingStatsService : IMarkingStatsService
{
    private readonly IDbContextFactory<DatabaseContext> _dbContextFactory;
    private readonly IWorklogEntryService _worklogEntryService;
    private readonly IWorklogTagRepository _worklogTagRepository;

    public MarkingStatsService(IDbContextFactory<DatabaseContext> dbContextFactory, IWorklogEntryService worklogEntryService, IWorklogTagRepository worklogTagRepository)
    {
        _dbContextFactory = dbContextFactory;
        _worklogEntryService = worklogEntryService;
        _worklogTagRepository = worklogTagRepository;
    }

    public async Task<IEnumerable<WeeklyValue>> GetOverheadByWeek(long userId, long projectId, long? sprintId = null)
    {
        await using var context = await _dbContextFactory.CreateDbContextAsync();

        var sprints = context.Sprints
            .Where(x => x.SprintProjectId == projectId)
            .Where(x => sprintId == null || x.Id == sprintId);
          
        var overheadEntries = await sprints
            .SelectMany(x => x.OverheadEntries)
            .Where(entry => entry.UserId == userId)
            .Include(overheadEntry => overheadEntry.Sprint)
            .ToListAsync();
  
        // Get all Mondays from weeks in returned sprint range
        var weekStarts = await CalculateDateRangesForSprintOrSprints(sprints, sprintId);
  
        // For each week in the range, get all instances of time spent per sprint
        var overheadEntriesPerSprint = overheadEntries.GroupBy(entry => entry.Sprint).ToList();
        return weekStarts.SelectMany(weekStart =>
        {
            var timesSpentInWeek = overheadEntriesPerSprint.Select(overheadEntriesInSprintGroup =>
            {
                var timeSpent = overheadEntriesInSprintGroup
                    .Where(entry => DateOnly.FromDateTime(entry.Occurred.AddDays(DayOfWeek.Monday - entry.Occurred.DayOfWeek)) == weekStart)
                    .Sum(entry => entry.DurationTicks);

                return new WeeklyValue
                {
                    WeekStart = weekStart,
                    Value = timeSpent,
                    SprintId = overheadEntriesInSprintGroup.Key.Id,
                    SprintName = overheadEntriesInSprintGroup.Key.Name,
                };
            }).ToList();
            return timesSpentInWeek.Any() ? timesSpentInWeek : new List<WeeklyValue> { new() { WeekStart = weekStart } };
        });
    }

    public async Task<IEnumerable<WeeklyValue>> GetAvgWorkLogDurationByWeek(long userId, long projectId, long? sprintId)
    {
        await using var context = await _dbContextFactory.CreateDbContextAsync();

        var sprints = await GetSprints(projectId, sprintId, context).ToListAsync();
        
        var workLogs = await _worklogEntryService.GetWorklogEntriesForProjectAsync(projectId, sprintId, userId);
        
        // Get all Mondays from weeks in returned sprint range
        var weekStarts = await CalculateDateRangesForSprintOrSprints(sprints, sprintId);
  
        // For each week in the range, get all instances of time spent per sprint
        var workLogEntries = workLogs.ToList();
        var workLogsPerSprint= workLogEntries.GroupBy(x => x.Task.UserStory.StoryGroupId).ToList();
        return weekStarts.SelectMany(weekStart =>
        {
            var timesSpentInWeek = workLogsPerSprint.Select(workLogsInSprintGroup =>
            {
                var currentSprintId = workLogsInSprintGroup.Key;
                long averageLogDuration;
                var logsForWeek = workLogsInSprintGroup
                    .Where(entry => DateOnly.FromDateTime(entry.Occurred.AddDays(DayOfWeek.Monday - entry.Occurred.DayOfWeek)) == weekStart).ToList();

                if (sprintId != null && logsForWeek.Any())
                {
                    averageLogDuration = (long) logsForWeek
                        .Select(entry => entry.GetTotalTimeSpent().Ticks)
                        .Average();
                }
                else
                {
                    var totalTimeSpent = logsForWeek.Sum(entry => entry.GetTotalTimeSpent().Ticks);
                    // Divide either by the number of work logs this week from all sprints, or by 1 if there are none
                    var numberOfWorkLogsInWeekFromAllSprints = workLogEntries.Count(entry => DateOnly.FromDateTime(entry.Occurred.AddDays(DayOfWeek.Monday - entry.Occurred.DayOfWeek)) == weekStart);
                    var denominator = numberOfWorkLogsInWeekFromAllSprints > 0 ? numberOfWorkLogsInWeekFromAllSprints : 1;
                    averageLogDuration = totalTimeSpent / denominator;
                }
                
                return new WeeklyValue
                {
                    WeekStart = weekStart,
                    Value = averageLogDuration,
                    SprintId = currentSprintId,
                    SprintName = sprints.First(x => x.Id == currentSprintId).Name
                };
            }).ToList();
            return timesSpentInWeek.Any() ? timesSpentInWeek : new List<WeeklyValue> { new() { WeekStart = weekStart } };
        });
    }
    
    public Task<IEnumerable<WeeklyValue>> GetStoryHoursByWeek(long userId, long projectId, long? sprintId = null)
    {
        return GetTaggedHoursByWeek(userId, projectId, WorklogTagUtils.StoryWorklogNames, sprintId);
    }

    public Task<IEnumerable<WeeklyValue>> GetTestHoursByWeek(long userId, long projectId, long? sprintId = null)
    {
        return GetTaggedHoursByWeek(userId, projectId, WorklogTagUtils.TestWorklogNames, sprintId);
    }
    
    public Task<IEnumerable<WeeklyValue>> GetDevHoursByWeek(long userId, long projectId, long? sprintId = null)
    {
        return GetTaggedHoursByWeek(userId, projectId, WorklogTagUtils.DevWorklogNames, sprintId);
    }

    private async Task<IEnumerable<WeeklyValue>> GetTaggedHoursByWeek(long userId, long projectId, IEnumerable<string> tagNames, long? sprintId = null)
    {
        await using var context = await _dbContextFactory.CreateDbContextAsync();

        var sprints = await context.Sprints
            .Where(x => x.SprintProjectId == projectId)
            .Where(x => sprintId == null || x.Id == sprintId)
            .ToListAsync();
        
        // Filter work logs by tags, users, and sprints ranges
        var dbWorklogTags = tagNames.Select(x => _worklogTagRepository.GetByNameAsync(x));
        var worklogTags = await Task.WhenAll(dbWorklogTags);
        
        var user = await context.Users.FirstOrDefaultAsync(x => x.Id == userId);
        var startAndEndDate = await CalculateDateRangesForSprintOrSprints(sprints);
        var workLogEntryFilter = new WorklogEntryFilter
        {
            WorklogTagsFilter = worklogTags.ToArray(), 
            AssigneeFilter = user != null ? [user] : [], 
            DateRangeStart = startAndEndDate.Min(), 
            DateRangeEnd = startAndEndDate.Max(), 
            DateRangeFilterEnabled = true
        };
        
        var workLogs = await _worklogEntryService.GetByProjectFilteredAsync(projectId, workLogEntryFilter, sprintId);

        return await ExtractWeeklyTimeSpanFromWorklogsForSprint(
            sprintId, 
            sprints, 
            workLogs, 
            w => new TimeSpan(w.Sum(entry => entry.TaggedWorkInstances.Where(instance => worklogTags.Contains(instance.WorklogTag)).Sum(instance => instance.Duration.Ticks)))
        );
    }
    
    public async Task<IEnumerable<WeeklyValue>> GetShortestWorklogDurationByWeek(long userId, long projectId, long? sprintId = null)
    {
        await using var context = await _dbContextFactory.CreateDbContextAsync();

        var worklogEntries = await context.WorklogEntries
            .Where(x => x.UserId == userId)
            .Where(x => x.Task.UserStory.ProjectId == projectId)
            .Where(x => sprintId == null || x.Task.UserStory.StoryGroupId == sprintId)
            .Include(x => x.TaggedWorkInstances)
            .Include(x => x.Task).ThenInclude(x => x.UserStory)
            .ToListAsync();
        
        var sprints = await context.Sprints
            .Where(x => x.SprintProjectId == projectId)
            .Where(x => sprintId == null || x.Id == sprintId)
            .ToListAsync();

        return await ExtractWeeklyTimeSpanFromWorklogsForSprint(
            sprintId, 
            sprints, 
            worklogEntries,
            worklogs => worklogs.Any() ? worklogs.Min(x => x.GetTotalTimeSpent()) : new TimeSpan(0)
        );
    }

    public async Task<IEnumerable<WeeklyValue>> GetTestingContributionByWeek(long userId, long projectId, long? sprintId)
    {
        await using var context = await _dbContextFactory.CreateDbContextAsync();

        var testHoursByWeek = await GetTestHoursByWeek(userId, projectId, sprintId);
        var devHoursByWeek = await GetDevHoursByWeek(userId, projectId, sprintId);

        var weeklyTimeSpans = devHoursByWeek.ToList();
        var hoursByWeek = testHoursByWeek.ToList();
        
        var firstWeek = hoursByWeek.Min(x => x.WeekStart) < weeklyTimeSpans.Min(x => x.WeekStart)
            ? hoursByWeek.Min(x => x.WeekStart)
            : weeklyTimeSpans.Min(x => x.WeekStart);
        var lastWeek = hoursByWeek.Max(x => x.WeekStart) > weeklyTimeSpans.Max(x => x.WeekStart)
            ? hoursByWeek.Max(x => x.WeekStart)
            : weeklyTimeSpans.Max(x => x.WeekStart);

        var weeklyTestingContributions = new List<WeeklyValue>();

        var currentWeek = firstWeek;
        
        while (currentWeek <= lastWeek)
        {
            var newWeeklyTestContr = hoursByWeek.Where(x => x.WeekStart == currentWeek).Select(x =>
            {
                var testHoursThisWeek = hoursByWeek.Single(a => a.WeekStart == currentWeek).Value;
                var devHoursThisWeek = weeklyTimeSpans.Single(b => b.WeekStart == currentWeek).Value;
                
                return x with { Value = testHoursThisWeek != 0.0 ? testHoursThisWeek / (testHoursThisWeek + devHoursThisWeek) : 0, WeekStart = currentWeek };
            });
            
            weeklyTestingContributions.AddRange(newWeeklyTestContr);
            
            currentWeek = currentWeek.AddDays(7);
        }
        
        return weeklyTestingContributions;
    }

    private async Task<IEnumerable<WeeklyValue>> ExtractWeeklyTimeSpanFromWorklogsForSprint(
        long? sprintId, 
        IReadOnlyCollection<Sprint> sprints, 
        IEnumerable<WorklogEntry> workLogs,
        Func<ICollection<WorklogEntry>, TimeSpan> extractTimeSpanFunc
    ) {
        // Get all Mondays within returned sprint range
        var weekStarts = await CalculateDateRangesForSprintOrSprints(sprints, sprintId);

        // For each week in the range, get all instances of time spent per sprint
        var workLogsPerSprint = workLogs.GroupBy(x => x.Task.UserStory.StoryGroupId).ToList();

        return weekStarts.SelectMany(weekStart =>
        {
            var timesSpentInWeek = workLogsPerSprint.Select(workLogsInSprintGroup =>
            {
                var worklogs = workLogsInSprintGroup
                    .Where(entry => DateOnly.FromDateTime(entry.Occurred.AddDays(DayOfWeek.Monday - entry.Occurred.DayOfWeek)) == weekStart)
                    .ToList();
                var timeSpent = extractTimeSpanFunc(worklogs);

                var currentSprintId = sprintId ?? workLogsInSprintGroup.Key;
                return new WeeklyValue
                {
                    WeekStart = weekStart,
                    Value = timeSpent.Ticks,
                    SprintId = currentSprintId,
                    SprintName = sprints.First(x => x.Id == currentSprintId).Name
                };
            }).ToList();
            return timesSpentInWeek.Any() ? timesSpentInWeek : [new WeeklyValue { SprintId = sprintId, SprintName = sprints.FirstOrDefault(x => x.Id == sprintId)?.Name, WeekStart = weekStart }];
        });
    }

    public async Task<IList<DateOnly>> CalculateDateRangesForSprintOrSprints(IEnumerable<Sprint> sprints, long? sprintId = null)
    {
        sprints = sprints.ToList();

        var start = sprints.Min(x => x.StartDate);
        var end = await GetSprintEndOrLastLog(sprints.MaxBy(x => x.EndDate));
        
        if (sprintId == null) return IsoWeekCalculator.GetWeekStartsBetweenDates(start, end);
        
        var sprint = sprints.First(x => x.Id == sprintId);
        start = sprint.StartDate;
        end =  await GetSprintEndOrLastLog(sprint);
        
        return IsoWeekCalculator.GetWeekStartsBetweenDates(start, end);
    }

    public async Task<DateOnly> GetSprintEndOrLastLog(Sprint sprint)
    {
        await using var context = await _dbContextFactory.CreateDbContextAsync();

        var overheadOccurrences = context.OverheadEntries
            .Where(x => x.SprintId == sprint.Id);

        if (!overheadOccurrences.Any()) return sprint.EndDate;

        var latestOverheadOccurence = DateOnly.FromDateTime(overheadOccurrences.Select(x => x.Occurred).Max());
        return latestOverheadOccurence > sprint.EndDate ? latestOverheadOccurence : sprint.EndDate;
    }

    private static IQueryable<Sprint> GetSprints(long projectId, long? sprintId, DatabaseContext context)
    {
        var sprints = context.Sprints
            .Where(x => x.SprintProjectId == projectId)
            .Where(x => sprintId == null || x.Id == sprintId);
        return sprints;
    }
}